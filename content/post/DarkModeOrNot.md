---
title: "你應該使用 Dark Mode 嗎？"
date: 2020-10-04T22:59:08+08:00
cover: "/images/logo.jpg"
tags: ["develope", "life"]
draft: false
---

## 前言

上週 Google Map 更新加入了 Dark Mode  
但在使用幾天後發現還是很難適應  
感覺在 {{<hl 0 "Dark Mode 下辨識度極差" >}}  
特此找資料好好認識 Dark Mode  
如果你沒有時間，可以直接跳到[結論](#結論)

<!--more-->

## 研究

除了 Google Map 的問題  
剛好我也看到推友的一篇[推文](https://twitter.com/ruanyf/status/1312556886412017665)  
也是討論 Dark Mode 不易閱讀  
以下是找資料整理的重點

第一篇文章 [iOS 13 深色模式能護眼、工作更專注？國外研究這樣看](https://www.bnext.com.tw/article/53683/dark-mode-care-eyes-health)

- Google 說 Dark Mode 的優點
  - 改善續航
  - 提高低視力和強光敏感者的可視性
- {{<hl 1 護眼的效果沒有答案 >}}
- {{<hl 2 螢幕傷眼的本質 >}}
  - {{<hl 2 藍光 >}}
  - {{<hl 2 頻閃 >}}

第二篇文章 [Is Dark Mode Better for Your Eyes?](https://rxoptical.com/eye-health/is-dark-mode-better-for-your-eyes/)

- {{<hl 1 "可讀性 Light > Dark" >}}
- 眼睛疲勞
  - {{<hl 2 "低光下 Dark Mode 可以降低疲勞" >}}
  - Dark Mode 下高對比會造成更多疲勞
  - Dark Mode 下難以閱讀長文章

第三篇文章 [Dark Mode vs. Light Mode: Which Is Better?](https://www.nngroup.com/articles/dark-mode/)

- Light Mode 在多方面都有優勢，但其實沒有顯著的差異
- 在夜晚，小字體的易讀性依舊是 Light > Dark
- Light Mode 有可能更容易造成近視
- 視覺弱勢的人可能更喜歡 Dark Mode

## 討論 ＋ 實驗

在正常光亮環境下：

- {{<hl 1 "Light Mode 可以以更低的螢幕亮度使用 保護眼睛、提升可讀性" >}}
- {{<hl 1 "Dark Mode 可能因為螢幕需要更亮 造成眼睛傷害" >}}

Light Mode 的優點都非常的明確  
那是不是應該把 Coding 環境改成 Light Mode 呢  
秉持著實驗精神我改用了 Light Mode 一個下午  
我的答案是不見得  
{{<hl 0 "Coding 並不是在閱讀文章" >}}  
通常不用很清楚的去看文字  
Syntax Highlight 才是重點  
或許跟配色的設計也有關係  
以下是我自己的感受

- {{<hl 2 "Light Mode 下暗色 Highlight 很難辨識" >}}
- {{<hl 2 "Dark Mode 下亮色 Highlight 非常清晰" >}}

## 結論

- 亮度：根據環境光調節，能看清的狀況下越低越好
- 對比：60~80%的對比是最舒適的
- {{<hl 0 "散光患者：不要用 Dark Mode" >}}
- 文字
  - 一般環境 -> Light Mode
  - 低光環境 -> Dark Mode
- 圖片 -> Dark Mode
- Coding -> Dark Mode
- 省電 -> Dark Mode (螢幕要是 OLED 才有用)

最後我還是把手機開啟了 Dark Mode  
這樣可以防止在黑夜中閃瞎眼  
當我覺得亮度不夠  
以往第一個反應是調高亮度  
但現在我會先關閉 Dark Mode

## 後記

在找資料的過程中  
也順便研究投影片要不要使用 Dark Mode  
發現了一個非常棒的影片[簡報背景該選亮色還是暗色？ | 10 分鐘學簡報](https://www.youtube.com/watch?v=lz4FnypZMuk)  
簡單的幾點包含

- 投影機 -> Light Mode
- 螢幕 -> Dark Mode
- 圖片 -> Dark Mode

影片中還有非常多的細節  
給 Designer 一個 Respect !
