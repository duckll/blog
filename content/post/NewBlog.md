---
title: "Blog 搬家囉"
author: DuckLL
date: 2019-08-08T22:41:45+08:00
tags: ["website", "hugo"]
draft: false
---

## 前言

自從 Google+關掉之後  
我的 Blogger 整個壞了  
所有的留言全部消失了  
新的留言我也沒有辦法回覆  
真的非常非常的難過 QAQ

因此決定建立一個新的 Blog  
在比較各大平台後  
決定改用 SSG 架設 Blog  
搬文章是很累人的事  
我只搬幾篇有討論度的文章過來  
舊的 Blog 也不會關閉  
但會體醒訪客到新站來交流討論

<!--more-->

## 搬家過程

SSG (Static Site Generator) 能幫你輕鬆產出內容豐富的靜態網站  
可以參考 [StaticGen](https://www.staticgen.com/)，而我選擇了[Hugo](https://gohugo.io/)

- 安裝容易
- 上手簡單
- 很多漂亮的主題(有些支援 disqus)
- Markdown 撰寫
- Shortcode 超神
- Gitlab 原生支援

並且搭配主題 [Hugo-nuo](https://github.com/laozhu/hugo-nuo)、[hly0928 的模改](https://blog.hly0928.com/post/blog-update-01/)  
再修改一些些的 CSS 樣式，最後發佈到 Gitlab 上。

### [Talk is cheap. Show me the code](https://gitlab.com/duckll/blog)
