---
title: "我想學資安 從哪裡開始呢 "
author: DuckLL
date: 2017-11-15T00:00:00+08:00
tags: ["security"]
draft: false
---

一直以來嚮往成為駭客的人就不少  
最近不少大學部的學弟妹來敲我  
"我想學資安 該從哪裡開始呢"  
網路上雖然資料多 但卻不知道從何看起？  
決定來分享自己的經歷跟想法吧  
{{< color lightgreen 歡迎各位分享這篇文章給對資安有興趣的朋友囉 >}}  
如果覺得文章有任何建議也可以底下留言互相交流

<!--more-->

首先要先了解資安的各項領域  
接者以三個面向  
{{< color lightpink "參加社群活動、大量閱讀、打 CTF" >}}  
持之以恆地去努力實現  
相信資安實力就能有所提升

## 資安抓周

人寫的程式不會是完美無暇  
因此資安不會有止息的一天  
資安問題與程式是一起產生的  
只要有新的程式就會有新的威脅  
幾乎每個領域都能會有對應的資安需求  
不過大致上還是能把資安分成幾個領域  
{{< color lightpink "密碼學、網路安全、網頁安全、逆向工程、程式安全、數位鑑識" >}}  
每個領域可以互相結合 建議初學者多方接觸尋找興趣  
如果想成為資安研究員 就需要具備各項領域的基礎知識  
如果想成為駭客 那你應該選擇一個領域去精通

### 密碼學 Cryptography

應具備的知識：離散數學  
未來的方向：通訊相關、影像加解密、區塊鏈  
代表攻擊：Padding Oracle、Hash Collision

算是**資訊安全的基礎**  
很多的資訊安全性都是建立在密碼學上  
大學部應該都會有相關課程可以去修習  
算是一定要了解的領域  
讓你在保護資料時不會出糗

### 網路安全 Network Security

應具備的知識：密碼學、網路概論  
未來的方向：網路安全管理、網路安全設備架設  
代表攻擊：DDoS、KRACK

這是許多網路攻擊發生的第一戰場  
主要是在**探討各種通訊協定**  
考量優缺點、應用的場景、權限的管理

### 網頁安全 Web Security

應具備的知識：前後端語言、伺服器設定  
未來的方向：滲透測試、Bug Bounty  
代表攻擊：XSS、SSRF

網頁安全是最容易入門的領域  
因為網頁屬於 Application 層  
存在的漏洞通常比較容易理解  
通常需要**大量的經驗以及跳脫性的思考(🍊:猥瑣流)**  
現實中存在的漏洞也比較多能挖掘  
能帶給初學者不錯的回饋與成就感  
但是..."入門簡單精通難"

### 逆向工程 Reverse engineering

應具備的知識：組合語言、計算機結構  
未來的方向：惡意程式分析  
代表攻擊：Malware、Software Cracking

因為需要的知識比較多  
算是入門難度比較高的領域  
通常會從**外掛開始下手**  
動手破解一些小遊戲  
學習動、靜態去分析一個執行檔  
可以為生活多不少樂趣?!

### 程式安全 Pwnable

應具備的知識：逆向工程、作業系統  
未來的方向：程式漏洞挖掘、軟體測試  
代表攻擊：DirtyCOW、Eternalblue

知識層面又更廣了  
通常需要透過逆向找出可能存在漏洞的程式碼  
並且想辦法**控制程式的流程 拿到電腦的控制權**  
跟系統用的 Library 還有系統特性有緊密的關係  
病毒或網路攻擊 會以 Pwn 做提權 來造成更大的威脅

### 數位鑑識 Forensic

應具備的知識：網路安全、逆向工程  
未來的方向：數位鑑識、資料救援  
代表攻擊：第一銀行(?

最近越來越熱門的領域  
因為 APT(標的式)的精密攻擊越來越多  
當造成的威脅已經成為事實  
亡羊補牢的方式就是透過鑑識  
找出被攻擊的弱點並加以補強

## 參與社群活動

參與社群活動可以讓你{{< color lightpink 維持對資安的熱誠 >}}  
這也是為什麼我把它擺在第一項  
時間是有限的 不可能學會所有的東西  
將知識內化後與他人交流達到有效的學習  
在學習的路上孤單是最大的障礙(不管什麼領域  
如果有人能跟你討論 讓你學習 那進步的速度一定很快  
多參加資安活動 除了增加自己的知識之外  
增加人脈 尋找志同道合的人更是重要

### UCCU

是**南部最大資安社群**  
主要據點在高第一  
定期會舉辦聚會  
通常是入門的資安 Talk

### BambooFox

交大的資安社團(除了交大還有其他幾間學校的學生  
裡面的成員臥虎藏龍有不少大神  
平常可以去參加他們的社課  
**社課是開給新手的課程** 不太需要擔心聽不懂  
如果想要深入的技術 可以跟他們一起打 CTF  
看大神把題目解開 偷偷學兩招就好(不要偷 FLAG 喔 X

### TDOH

全台最大學生資安社群  
特色就是吃 聚會食物都超棒  
會有定期聚辦在全台各地  
每年也會有一個 TDOH Conf  
提供學生一個**便宜又大碗的資安知識**  
另外週報下方會列出許多資安活動資訊

### RAT

**這是我第一個加入的社群**  
台南的資安社群 目前比較少辦聚會  
很感謝他們之前的逆向系列課程  
有人帶入門的感覺真的很好

### AIS3

這是教育部的計畫  
**每年會在暑假透過 CTF 篩選出有潛力的學生**  
並且在一兩個禮拜內精實的上課  
可以學習到很多東西 強烈建議可以去上一次  
pwn 的興趣就是在 2015 被 Sean 啟發  
目前資安人才培育計畫還有其他項目  
會在各地區舉辦資安技術課程

### HITCON

**台灣最大資安研討會**  
內容比較國際化 技術難度也比較高  
價錢對學生來說可能還是小貴？  
建議有強烈興趣的人來參加  
體驗這個資安年度盛會  
通常 R3 的議程比較適合初學者

## 大量的閱讀

資訊安全需要的訊息就是要最新最廣  
大致分成被動式的 RSS 與主動式論壇兩種方式  
還有資策會的 SecBuzzer 追最新的 twitter 趨勢

### RSS

如果還不知道什麼是 RSS 的可以去看我[之前的文章](https://www.duckll.tw/2017/04/fep.html)  
訂閱網站讓資訊搜集變得簡單  
我一天至少收到超過 50 則關於資安的文章  
再次強調 看到興趣的標題再點進去  
推薦的網站如下

- FreeBuf  
  資安界的 T 客邦
- Paper  
  技術質量都很高(不建議初學者看)
- 91ri  
  文章不太穩定，網站可以逛逛
- HITCONKB  
  HITCON 技術文章 Blog  
  順便宣傳大家多多投稿！
- TDOH  
  主要拿來收資安週報  
  資安週報算是一個大補帖  
  通常也是找有興趣的看

### 論壇

論壇是比較進階的部分  
提供不少現成的學習資源  
討論區會有破解的套路  
**內容貼近現實生活**  
技術質量普遍較高  
文章比較雜亂  
通常**找資料、找工具**才會使用

- i 春秋  
  每日精選值得一看  
  很多文章會轉貼到 Freebuf
- 看雪  
  老字號的安全團隊  
  裡面整理的資源不少  
  什麼領域應該都有討論
- 吾愛破解  
  一樣是老字號的論壇  
  主要是討論逆向工程還有軟體破解

## 打 CTF

Capture The Flag 是一種資安競賽  
透過各式資安能力來解題拿到 Flag  
打 CTF 就跟練球一樣  
{{< color lightpink 讓頭腦運轉維持你掌握的知識 >}}  
並且增加自己攻擊的思路

初學者推薦打常態 CTF

1. 嘗試自己解題 先不找資料
2. 如果遇到困難去找類似的 Writeup(平常打比賽的套路)  
   但當你覺得已經窮途末路 光靠自己的知識解不開時
3. 再去看看別人的 Writeup  
   或者當你今天解開了一道題目  
   你還是該去找別人的 Writeup  
   看看自己的想法跟別人的差在哪裡

下面這個整理了不少 CTF 網站  
http://captf.com/practice-ctf/  
初學者建議清單  
https://ctflearn.com/  
https://bamboofox.cs.nctu.edu.tw/  
https://hackme.inndy.tw  
http://pwnable.kr/play.php

CTF 的各種資源  
https://github.com/apsdehal/awesome-ctf  
CTF 的解題工具  
https://github.com/zardus/ctf-tools  
還有我自己的 CTF-BOX  
https://hub.docker.com/r/duckll/ctf-box/  
總結  
{{< color lightpink 玩資安最重要的就是興趣 >}}  
資訊更新速度都是其他領域的好幾倍  
稍微一不留神 自己的功力就會跟不上  
就像 CTF 的 PWN  
三年前 heap 大家看到跟鬼一樣  
三年後 heap 跟家常便飯一樣

如果以後想要找資安的工作  
除了摸熟 Linux 之外 Windows 也很重要喔  
(最近在點 Windows 技能...  
主動積極去面對各種挑戰吧  
{{< color lightgreen "希望大家都能快樂的學習資安！" >}}
