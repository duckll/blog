---
title: "關於本站"
layout: "about"
---

我是 DuckLL

是一名資安研究員

歡迎來到我的部落格

如果想看更多我之前的文章可以點[這裡](https://www.duckll.tw/)

---

My name is DuckLL.

I am a security researcher.

This is my new blog.

I have an old one on Blogger.

If you want to read my old posts, you can visit [here](https://www.duckll.tw/).
