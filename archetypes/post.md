---
title: "{{ replace .Name "-" " " | title }}"
author: {{ .Site.Params.author }}
date: {{ .Date }}
cover: "/images/logo.jpg"
tags: ["tag1", "tag2"]
draft: true
---
